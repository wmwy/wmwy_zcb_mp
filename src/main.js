// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'//引入store
import VueAwesomeSwiper from 'vue-awesome-swiper'
import _ from 'lodash'
import Swiper from "swiper"
import Reset from '../static/css/reset.css'
import swiper from '../static/css/swiper.min.css'

Vue.config.productionTip = true
Vue.use(VueAwesomeSwiper)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,//使用store
  components: { App },
  template: '<App/>'
})
