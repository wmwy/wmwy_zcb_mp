/**
 *      ┌─┐       ┌─┐
 *   ┌──┘ ┴───────┘ ┴──┐
 *   │                 │
 *   │       ───       │
 *   │  ─┬┘       └┬─  │
 *   │                 │
 *   │       ─┴─       │
 *   │                 │
 *   └───┐         ┌───┘
 *       │         │
 *       │         │
 *       │         │
 *       │         └──────────────┐
 *       │                        │
 *       │                        ├─┐
 *       │                        ┌─┘
 *       │                        │
 *       └─┐  ┐  ┌───────┬──┐  ┌──┘
 *         │ ─┤ ─┤       │ ─┤ ─┤
 *         └──┴──┘       └──┴──┘
 *                神兽保佑
 *               代码无BUG!
 */
import Vue 		from 'vue'
import Router 	from 'vue-router'
import index	from '@/components/index'//
import home		from '@/components/home/home'//首页
import service	from '@/components/home/service/service'//客服
import carBatter from '@/components/home/rent/car-batter'//租车_电状态

import user     from '@/components/home/user/user'// 用户中心
import userinfo from '@/components/home/user/user-info/user-info'// 押金明细
import QRcode from '@/components/home/user/QR-code/QR-code'// 押金明细
import mybonus  from '@/components/home/user/bonus/mybonus'// 我的奖金
import mydeposit from '@/components/home/user/deposit/mydeposit'// 押金明细
import inlease from '@/components/home/user/lease/in-lease'// 押金明细 wit_recordswit_records
import witrecords from '@/components/book/wit_records.vue'// 押金明细 wit_recordswit_records

import undoneorder from '@/components/Allorder/undone-order'// 未完成 订单
import endorder from '@/components/Allorder/end-order'// 已完成 订单

import renew	 from '@/components/return-renew/renew'// 续租
import returncb	 from '@/components/return-renew/return-CB'// 还车/还电

import account 	from '@/components/account/account'// 账户 主
import modifyworld 	from '@/components/account/password/modifyworld'// 账户 主

import login 	from '@/components/account/login'// 子 登入new-user
import lication from '@/components/account/lication'// 子 许可 新账户 骑手注册 / 非棋手注册
import vercard  from '@/components/account/verification/ver-card'// 子 许可 新账户 骑手注册 / 非棋手注册

import bookorder from '@/components/book/book-order'// 子 许可 新账户 骑手注册 / 非棋手注册
import selPlay from '@/components/book/selPlay'// selPlay z选择支付方式 

// ----------------- mods 无 router-link
import rentstatus from '@/components/mods/rent-status'//租车_电状态
import footoptioncar from '@/components/mods/foot-optioncar'//租车_
import face from '@/components/face/face.vue' // 空白页

// 支付成功 
import wcpay from '@/components/face/wcpay.vue' // 空白页

// 支付失败
import errorSb from '@/components/face/errorSb.vue' // 空白页

// ----------------- mods 无 router-link
Vue.use(Router)

export default new Router({
  routes: [
    {
    path: '/index',
    component: index,
	  redirect:'home',/*默认*/
	  children:[
		{ path:'/index/service',name: '联系客服', component: service},
		{ path:'/index/home',name: '首页', component: home},
		{ path:'/index/user',name: '个人中心', component: user}
	  ]
    },
    {// 用户 、 个人中心
      path: '/user',
      name: 'user',
      component: user
    },
    {// 账户
      path: '/',
      component: account,
      redirect:'login',/*默认*/
      children:[
        { path:'/login',name:'用户登入',component:login},
        { path:'/lication',name:'新用户',component:lication},
        { path:'/vercard',name:'用户认证',component:vercard},
      ]
    },
    {// modifyworld
      path: '/modifyworld',
      name: 'modifyworld',
      component: modifyworld,
    },
    {// selPlay
      path: '/selPlay/',
      name: 'selPlay',
      component: selPlay,
    },
    {//租车/租电池 
      path: '/carBatter',
      name: 'pathRouter',
      component: carBatter,
    },
    {//租车/租电池 订单 详情
      path: '/bookorder',
      name: 'bookorder',
      component: bookorder,
    },
    {// 续租
      path: '/renew',
      name: 'renew',
      component: renew,
    },
    {// returncb 还车/还电
      path: '/returncb',
      name: 'returncb',
      component: returncb,
    },
    {// 用户信息
      path: '/userinfo',
      name: 'userinfo',
      component: userinfo,
    },
    {// QRcode
      path: '/QRcode',
      name: 'QRcode',
      component: QRcode,
    },
    {// 奖金
      path: '/mybonus',
      name: 'mybonus',
      component: mybonus,
    },
    {// 押金
      path: '/mydeposit',
      name: 'mydeposit',
      component: mydeposit,
    },
    {// 租赁中mydeposit
      path: '/inlease',
      name: 'inlease',
      component: inlease,
    },
    {// 未完成 订单
      path: '/undoneorder',
      name: 'undoneorder',
      component: undoneorder,
    },
    {// 未完成 订单 
      path: '/endorder',
      name: 'endorder',
      component: endorder,
    },
    {// 提现纪录 witrecords 
      path: '/witrecords',
      name: 'witrecords',
      component: witrecords,
    },
    {// 空白页
      path: '/face',
      name: 'face',
      component: face,
    },
    {// 支付成功
      path: '/wcpay',
      name: '支付成功',
      component: wcpay,
    },
	{// 支付失败
	  path: '/errorSb',
	  name: '支付失败',
	  component: errorSb,
	}
	

  ]
})
