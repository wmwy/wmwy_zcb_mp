 
import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
 const state={   //要设置的全局访问的state对象  getChangedNum
	 url:'http://zcb.wmwy.com', //
     showFooter: true,
	 base1: 'http://local.admin.mb.411er.com',
	 forcedLogin: false, //是否需要强制 注册
	 hasLogin: true, //是否登入
	 is_token: '',
	 order_num: 'ZCB1912112209201718', // 订单号
	 order_price: '170000',// 订单 价格
	 userInfo: {},
   };
const getters = {   //实时监听state值的变化(最新状态)// 页面 使用 值 方法 this.$store.getters.base1 order_num order_price
    hasLogin(state) {  //承载变化的showFooter的值
       return state.hasLogin
    },
    getChangedNum(){  //承载变化的changebleNum的值
       return state.is_token
    },
	base1(state){
		return state.base1
	},
	url(state){
		return state.url
	},
	orderNum(state){  //承载变化的 // 订单号
	   return state.order_num
	},
	orderPrice(state){  //承载变化的  订单 价格
	   return state.order_price
	}
};
const mutations = { // 页面调用 方法 this.$store. commit ('hide') 
    show(state) {   //自定义改变state初始值的方法，这里面的参数除了state之外还可以再传额外的参数(变量或对象);
        state.showFooter = true;
    },
    hide(state) {  //同上
        state.showFooter = false;
    },
    is_token(state,sum){ //同上， this.$store. commit ('is_token',num)
       state.is_token=sum;
    },
	order_num(state,sum){ //同上， this.$store. commit ('is_token',num)
	   state.order_num = sum;
	},
	order_price(state,sum){ //同上， this.$store. commit ('is_token',num)
	   state.order_price = sum;
	}
};
 const actions = {
    hideFooter(context) {  //自定义触发mutations里函数的方法，context与store 实例具有相同方法和属性
        context.commit('hide');
    },
    showFooter(context) {  //同上注释
        context.commit('show');
    },
    getNewNum(context,num){   //同上注释，num为要变化的形参  this.$store.dispatch('getNewNum'，6) //6要变化的实参
        context.commit('is_token',num)
    },
	getOrderId(context,num){   //同上注释，num为要变化的形参  this.$store.dispatch('getOrderId'，6) //6要变化的实参
	    context.commit('order_num',num)
	},
	getOrderPri(context,num){   //同上注释，num为要变化的形参  this.$store.dispatch('getOrderPri'，6) //6要变化的实参
	    context.commit('order_price',num)
	}
};
  const store = new Vuex.Store({
       state,
       getters,
       mutations,
       actions
});
export default store;