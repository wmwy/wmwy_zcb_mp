import axios from 'axios';

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
import qs from 'qs';

// const base1 = 'http://local.admin.mb.411er.com';
const base1 = 'http://zcb.womiwuyou.com';  //线上服务器
// const base1 = 'http://zcb.wmwy.com';
// const base1 = 'http://bc5bb3b6.ngrok.io';
// http request 拦截器
axios.interceptors.request.use(
  config => {
    console.log(config);
    if (sessionStorage.getItem("token")) { // 判断是否存在token，如果存在的话，则每个http header都加上token
      config.headers.Authorization = sessionStorage.getItem("token"); //请求头加上token
    }
    if (config.method == 'get') {
      //axios中get请求会移除Content-Type，此处是绕过判断给get添加header
      config.data = true;
      config.headers['Content-Type'] = 'application/json'
    }
    return config
  },
  err => {
    return Promise.reject(err)
  }
)
// http response 拦截器
axios.interceptors.response.use(
  response => {
    //拦截响应，做统一处理
    return response
  },
  //接口错误状态处理，也就是说无响应时的处理
  error => {
    return Promise.reject(error) // 返回接口返回的错误信息
  }
)

let http = axios.create({
  //baseURL: 'http://zcb.wmwy.com/',
  baseURL: 'localhost:8080/',
  withCredentials: true,
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
  },

  transformRequest: [function(data) {
    let newData = '';
    for (let k in data) {
      if (data.hasOwnProperty(k) === true) {
        newData += encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) + '&';
      }
    }
    return newData;
  }]
});
const  defaultHeaders = {
    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
};
 const  defaultHeadertoo = {
    'Content-Type': 'application/json;charset=UTF-8',
};

export default {
  baseUrl: base1
}
//获取首页数据
export const indexView = params => {
  return axios.post(`${base1}/index/indexView`, {
    params: params
  }, defaultHeaders);
};

export const leaseView = params => {
  return axios.post(`${base1}/index/leaseView`, {
    params: params
  }, defaultHeaders);
};

export const rentView = params => {
  return axios.post(`${base1}/order/rentView`, qs.stringify(params), defaultHeaders);
}; // 向服务器 发送参数

export const login = params => {
  return axios.post(`${base1}/login/login`, qs.stringify(params) , defaultHeaders);
};
export const wechatLogin = params => {
  return axios.post(`${base1}/login/wechatLogin`, {
    params: params
  }, defaultHeaders);
};
export const wechatAuthorized = params => {
  return axios.post(`${base1}/login/wechatAuthorized`, qs.stringify(params), defaultHeaders);
};
export const getUserInfo = params => {
  return axios.post(`${base1}/login/getUserInfo`, params, defaultHeaders);
};

export const register = params => { return axios.post(`${base1}/agent/register`, params); };

export const createOrder = params => {
  return axios.post(`${base1}/order/createOrder`, params);
};

// 续租  接口
export const reletView = params => {
  return axios.post(`${base1}/order/reletView`, params);
};
// 续租 订单提交 接口
export const createReletOrder = params => {
  return axios.post(`${base1}/order/createReletOrder`, params);
};

// 还车 还电 接口 获取 参数 /finance/
export const reservationView = params => {
  return axios.post(`${base1}/order/reservationView`, params);
};
// 还车 还电 接口 发送 参数 /finance/
export const timingRepayOrder = params => {
  return axios.post(`${base1}/order/timingRepayOrder`, params);
};

// 提现纪录??? ????? 这是什么 ?
export const withdrawalRecord = params => {
  return axios.post(`${base1}/finance/withdrawalRecord`, params);
};


export const ordersListView = params => {
  return axios.post(`${base1}/order/ordersListView`, params);
};

// /order/cancelOrder 取消订单接口
export const cancelOrder = params => {
  return axios.post(`${base1}/order/cancelOrder`, qs.stringify(params), defaultHeaders);
}; // 向服务器 发送参数

// 所有 押金 奖金纪录 	//
export const getAgentAccount = params => {
  return axios.post(`${base1}/finance/getAgentAccount`, params);
};

///finance/depositRecord  押金纪录 type
export const depositRecord = params => {
  return axios.post(`${base1}/finance/depositRecord`, params);
};
// /finance/bonusRecord 奖金 纪录
export const bonusRecord = params => {
  return axios.post(`${base1}/finance/bonusRecord`, params);
};
// 提现纪录 /finance/toWithdrawal
export const toWithdrawal = params => {
  return axios.post(`${base1}/finance/toWithdrawal`, params);
};

export const payOrder = params => {
  return axios.post(`${base1}/order/payOrder`, qs.stringify(params), defaultHeaders);
}; // 公司值付

// export const wxOrder = params => {
//   return axios.post(`${base1}/weapplet/create`, qs.stringify(params), defaultHeaders);
// }; // 公司值付


export const wxOrder = params => {
  return axios.post(`${base1}/pay/create`, params);
}; ///weapplet/pay/create 

export const getCompanyList = params => { 
  return axios.post(`${base1}/agent/getCompanyList`, qs.stringify(params), defaultHeaders);
};


export const getRealNameUrl = params => {
  return axios.post(`${base1}/login/getRealNameUrl`,params);
}; // 向服务器 发送参数

// /login/getRealNameResult
export const getRealNameResult = params => { // 实名认证 成功
  return axios.post(`${base1}/login/getRealNameResult`, qs.stringify(params), defaultHeaders);
}; // 向服务器 发送参数

// 个人信息
export const getUserInfoView = params => {
  return axios.post(`${base1}/index/getUserInfoView`,params);
};

// /index/serviceView / 个人信息 修改
export const updateAgentCompanyInfo = params => { // 实名认证 成功
  return axios.post(`${base1}/index/updateAgentCompanyInfo`, qs.stringify(params), defaultHeaders);
};

// /index/serviceView 客服
export const serviceView = params => { // 实名认证 成功 /index/updateAgentPassWord
  return axios.post(`${base1}/index/serviceView`,params);
};

// /index/updateAgentPassWord
export const updateAgentPassWord = params => { // 实名认证 成功  newPassWord，oldPassWord
  return axios.post(`${base1}/index/updateAgentPassWord`, qs.stringify(params), defaultHeaders);
};

//  /order/takeOrder
export const takeOrder = params => { // 实名认证 成功  newPassWord，oldPassWord
  return axios.post(`${base1}/order/takeOrder`, qs.stringify(params), defaultHeaders);
};